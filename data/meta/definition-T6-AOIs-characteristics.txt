# Names of the characteristics (i.e., names of the columns) which are extracted from an input text file for each defined AOI.
# Each characteristic name is defined in a separate line. All white-space lines or lines starting with the '#' character are ignored.

#1 number of fixations in each AOI
n_fix_aoi

#2 av fixation duration - average fixation duration in each AOI
mean_fix_dur_aoi

#3 revisits - number of returns to each AOI
n_revisits_aoi

#4 first fixation duration in each AOI
first_fix_dur_aoi

#5 transitions between AOIs - each trespassing saccade between any AOIs (on the lowest level, that we will use, either between item-AOIs or even sub-item AOIs)
n_transit_trial
