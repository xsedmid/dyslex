# Names of the characteristics (i.e., names of the columns) which are extracted from an input text file.
# Each characteristic name is defined in a separate line. All white-space lines or lines starting with the '#' character are ignored.
# The first line has to contain the fixed number of lines contained in the input text file (this is similar to the AOI characteristics where the number of lines in the input text file has to correspond to the number of pre-defined AOIs).

# number of lines in the input text file
60

#1 saccadic onset latency (of the first response) - from the appearance of the target to the start of the first (main) saccade 
saccadic_onset_latency_trial

#2 peak saccadic velocity (of the "main saccade")
peak_saccadic_velocity_trial
