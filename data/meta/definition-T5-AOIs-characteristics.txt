# Names of the characteristics (i.e., names of the columns) which are extracted from an input text file for each defined AOI.
# Each characteristic name is defined in a separate line. All white-space lines or lines starting with the '#' character are ignored.

#1 number of fixations - total number of fixations per AOI (line/word)
n_fix_aoi

#2 av fixation duration - average fixation duration per AOI (line/word)
mean_fix_dur_aoi

#3 revisits - number of returns to each AOI
n_revisits_aoi

#4 landing position of first fixation in AOI - % part of the AOI
first_fix_land_pos_aoi
