# Names of the global characteristics which are extracted from an input text file. A global characteristic has to have the same value in all the text-file lines, so its value is taken from the first data line.
# Each characteristic name is defined in a separate line. All white-space lines or lines starting with the '#' character are ignored.

#1 total number of hypermetric saccades - main sacc. overshooting the target
n_hypermetric_sacc_task

#2 total number of hypometric saccades - main sacc. undershooting the target
n_hypometric_sacc_task
